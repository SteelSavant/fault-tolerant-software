﻿using System.Collections;
using System.Collections.Generic;

//Not terribly good implementation of a concurrent list. Should suit my purposes though.

namespace FTStorage
{
    class ConcurrentList<T> : IEnumerable<T>
    {
        private List<T> insideList = new List<T>();

        public void Add(T element)
        {
            lock (insideList)
            {
                insideList.Add(element);
            }
        }

        public void Remove(T element)
        {
            lock (insideList)
            {
                insideList.Remove(element);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            lock (insideList)
            {
                return ((IEnumerable<T>)insideList).GetEnumerator();
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            lock (insideList)
            {
                return ((IEnumerable<T>)insideList).GetEnumerator();
            }
        }

        public ConcurrentList<T> Copy()
        {
            var ret = new ConcurrentList<T>();
            lock (insideList)
            {
                ret.insideList = new List<T>();
                ret.insideList.AddRange(insideList);
            }
            return ret;
        }
    }
}
