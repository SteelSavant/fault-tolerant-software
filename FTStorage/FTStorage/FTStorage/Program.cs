﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Linq;
using System.IO;
using System.Text;

//Everything is little endian

namespace FTStorage
{
    public class StorageClient
    {
        int FILE_COUNT;
        const int SEGMENT_SIZE = 10;
        const int SEND_PORT = 1982;
        const int RECIEVE_PORT = 1983;

        const string READ_DIRECTORY = "D:/Desktop/Fault Tolerant Software/FTStorage/TestFiles/";
        const string WRITE_DIRECTORY = "D:/Desktop/Fault Tolerant Software/FTStorage/Output/";

        UdpClient client = new UdpClient(RECIEVE_PORT);
        IPEndPoint sendEndPoint = new IPEndPoint(IPAddress.Loopback, SEND_PORT);
        IPEndPoint recieveEndPoint = new IPEndPoint(IPAddress.Loopback, RECIEVE_PORT);

        int opPointer = 0;
        string opFile = "";

        ConcurrentDictionary<string, int> files = new ConcurrentDictionary<string, int>();
        ConcurrentList<Response> messages = new ConcurrentList<Response>();

        StorageClient()
        {
            Thread messageThread = new Thread(Recieve);
            messageThread.Start();

            Thread integrityThread = new Thread(IntegrityCheck);
            integrityThread.Start();

            Ops();
        }

        void Recieve()
        {
            while (true)
            {
                var response = new Response(client.Receive(ref recieveEndPoint));
                messages.Add(response);
            }
        }

        void Ops()
        {
            while (true)
            {
                Console.WriteLine("Read (R), Write (W), or Test(T)?");
                var choice = char.ToLower(Console.ReadKey().KeyChar);
                if (choice == 'r')
                {
                    Console.WriteLine("\nEnter file to be read");
                    var file = Console.ReadLine();
                    ReadFile(file);
                }
                if (choice == 'w')
                {
                    Console.WriteLine("\nEnter the file to write");
                    var fileName = Console.ReadLine();
                    WriteFile(fileName);
                }
                if(choice == 't')
                {
                    Console.WriteLine("\nEnter which test to run (1-6):" +
                        "\n1) Read Failure: 25%, Write Failure: 25%, Corruption Timer: 2000, Corruption Chance:  50%" +
                        "\n2) Read Failure:  0%, Write Failure:  0%, Corruption Timer: 1000, Corruption Chance: 100%" +
                        "\n3) Read Failure: 10%, Write Failure: 10%, Corruption Timer: 2000, Corruption Chance:  25%" +
                        "\n4) Read Failure:  0%, Write Failure:  0%, Corruption Timer: 5000, Corruption Chance:  75%" +
                        "\n5) Read Failure: 25%, Write Failure: 25%, Corruption Timer: 5000, Corruption Chance: 100%" +
                        "\n6) Read Failure: 95%, Write Failure: 95%, Corruption Timer:  100, Corruption Chance:  90%");
                    Console.WriteLine();

                    int.TryParse(Console.ReadKey().KeyChar.ToString(), out int test);
                    Console.WriteLine();

                    string file;
                    int seconds;

                    switch(test)
                    {
                        case 1:
                            file = "SmallTextFile.txt";
                            seconds = 30;
                            break;
                        case 2:
                            file = "LargeTextFile.txt";
                            seconds = 30;
                            break;
                        case 3:
                            file = "SmallBinary.jpg";
                            seconds = 30;
                            break;
                        case 4:
                            file = "LargeBinary.wav";
                            seconds = 30;
                            break;
                        case 5:
                            file = "SmallBinary.jpg";
                            seconds = 180;
                            break;
                        case 6:
                            file = "SmallTextFile.txt";
                            seconds = 15;
                            break;
                        default:
                            Console.WriteLine("Invalid Selection");
                            return;
                    }
                    Console.WriteLine($"Writing file {file}...");
                    WriteFile(file);
                    Console.WriteLine("Waiting...");
                    Thread.Sleep(seconds * 1000);
                    Console.WriteLine($"Reading file {file}...");
                    ReadFile(file);
                    Console.WriteLine($"Test {test} complete.");
                }
            }
        }

        void IntegrityCheck()
        {
            while (true)
            {
                //Thread.Sleep(200);
                foreach (var file in files.Keys)
                {
                    int len = file == opFile ? opPointer - SEGMENT_SIZE : files[file];
                    for (int checkPointer = 0; checkPointer < len; checkPointer += 10)
                    {
                        var data = ReadAll(file, checkPointer);
                        Fix(file, checkPointer, data);
                    }
                }
            }
        }

        void WriteFile(string fileName)
        {
            var file = READ_DIRECTORY + fileName;
            var bytes = File.ReadAllBytes(file);

            FILE_COUNT = bytes.Length > 2000000 ? 3 : 5;

            opFile = fileName;
            files[fileName] = bytes.Length;

            for (opPointer = 0; opPointer < bytes.Length - 10; opPointer += 10)
            {
                WriteAll(fileName, opPointer, bytes.Skip(opPointer).Take(SEGMENT_SIZE).ToArray());
            }

            WriteAll(fileName, opPointer, bytes.Skip(opPointer).Take(bytes.Length - opPointer).ToArray());
            opFile = "";
        }

        void ReadFile(string file)
        {
            if (!files.ContainsKey(file))
            {
                Console.WriteLine("File does not exist");
            }
            else
            {
                List<byte> readBuffer = new List<byte>();
                for (opPointer = 0; opPointer < files[file]; opPointer += 10)
                {
                    var bytes = ReadAll(file, opPointer);
                    var correct = Fix(file, opPointer, bytes);

                    readBuffer.AddRange(correct);
                }

                File.WriteAllBytes(WRITE_DIRECTORY + file, readBuffer.ToArray());
            }
        }

        byte[][] ReadAll(string file, int checkPointer)
        {
            const int MAX_ITERATIONS = 100000;
            
            var data = new byte[FILE_COUNT][];

            for (int i = 0; i < FILE_COUNT; ++i)
            {
                var fileName = CreateFileName(file, i);
                var message = CreateReadMessage(fileName, checkPointer);
                var valid = false;
                while (!valid)
                {
                    lock(client)
                        client.Send(message, message.Length, sendEndPoint);

                    var toRemove = new Response();
                    var count = 0;

                    while (toRemove.data == null && count < MAX_ITERATIONS)
                    {
                        var messages = this.messages.Copy();
                        foreach (var response in messages)
                        {
                            if (response.opCode == 'D' && response.fileName == fileName && response.location == checkPointer)
                            {
                                data[i] = response.data;
                                toRemove = response;
                                valid = response.valid;
                                break;
                            }
                        }
                        count++;
                    }
                    messages.Remove(toRemove);
                }
            }
            return data;
        }

        void WriteAll(string file, int location, byte[] data)
        {
            for(int i = 0; i < FILE_COUNT; ++i)
            {
                WriteSingle(file, i, location, data);
            }
        }

        void WriteSingle(string file, int index, int location, byte[] data)
        {
            const int MAX_ITERATIONS = 100000;

            var fileName = CreateFileName(file, index);
            var message = CreateWriteMessage(fileName, location, data);
            var valid = false;
            while (!valid)
            {
                lock (client)
                    client.Send(message, message.Length, sendEndPoint);
                var toRemove = new Response();
                var count = 0;

                while (toRemove.data == null && count < MAX_ITERATIONS)
                {
                    var messages = this.messages.Copy();
                    foreach (var response in messages)
                    {
                        if (response.opCode == 'A' && response.fileName == fileName && response.location == location)
                        {
                            valid = response.valid;
                            toRemove = response;
                            break;
                        }
                    }
                    count++;
                }
                messages.Remove(toRemove);
            }
        }

        byte[] Validate(byte[][] data)
        {
            var correct = new byte[data[0].Length];

            for (int byteIndex = 0; byteIndex < correct.Length; byteIndex++)
            {
                byte result = 0;
                for (int bitIndex = 0; bitIndex < 8; bitIndex++)
                {
                    var zeroCount = 0;
                    var oneCount = 0;

                    for (int fileIndex = 0; fileIndex < FILE_COUNT; fileIndex++)
                    {
                        var shift = (1 << bitIndex);
                        if ((data[fileIndex][byteIndex] & shift) == shift)
                        {
                            oneCount++;
                        }
                        else
                        {
                            zeroCount++;
                        }
                    }

                    if (oneCount > zeroCount)
                    {
                        result |= (byte)(1 << bitIndex);
                    }
                }

                correct[byteIndex] = result;
            }

            return correct;
        }

        byte[] Fix(string file, int location, byte[][] data)
        {
            var correct = Validate(data);

            for(int i = 0; i < FILE_COUNT; ++i)
            {
                if (!BitConverter.ToString(correct).Equals(BitConverter.ToString(data[i])))
                {
                    WriteSingle(file, i, location, correct);
                }
            }

            return correct;
        }

        byte[] CreateReadMessage(string fileName, int location)
        {
            var ret = new byte[37];
            ret[0] = (byte)'R';

            Buffer.BlockCopy(Encoding.ASCII.GetBytes(fileName), 0, ret, 1, fileName.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(location), 0, ret, 33, 4);

            return ret;
        }

        byte[] CreateWriteMessage(string fileName, int location, byte[] data)
        {
            var r = CreateReadMessage(fileName, location);
            var ret = new byte[48];

            ret[0] = (byte)'W';
            Buffer.BlockCopy(r, 1, ret, 1, r.Length - 1);
            ret[r.Length] = (byte)data.Length;
            Buffer.BlockCopy(data, 0, ret, r.Length + 1, data.Length);

            return ret;
        }

        string CreateFileName(string file, int index)
        {
            var split = file.Split('.');
            return $"{split[0]}_{index}.{split[1]}";
        }

        static void Main()
        {
            new StorageClient();
        }

        struct Response
        {
            public char opCode;
            public string fileName;
            public int location;
            public byte[] data;
            public bool valid;

            public Response(byte[] data)
            {
                opCode = (char)data[0];
                fileName = Encoding.ASCII.GetString(data.Skip(1).Take(32).ToArray()).Replace('\0', ' ').TrimEnd();
                location = BitConverter.ToInt32(data, 33);
                var length = (short)data[37];
                this.data = data.Skip(38).Take(length).ToArray();
                valid = BitConverter.ToBoolean(data, data.Length - 1);
            }
        }
    }
}
