﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace FTNetwork
{
    public partial class Chat : Form
    {
        int port = -1;
        bool done = false;
        bool stillRecieving = false;

        int sentCount = 0;
        int recievedCount = 0;
        int sentByOtherCount = 0;
        int recievedByOtherCount = 0;
        int highestRecieved = 0;
        int textLength = 0;
        int currentBlock = 0;

        UdpClient client;
        IPEndPoint endPoint;
        IPEndPoint recieveEndPoint;

        ConcurrentDictionary<int, byte[]> toSend = new ConcurrentDictionary<int, byte[]>();
        ConcurrentDictionary<int, DataMessage> recieved = new ConcurrentDictionary<int, DataMessage>();
        ConcurrentDictionary<DisplayKey, DataMessage> display = new ConcurrentDictionary<DisplayKey, DataMessage>();
        List<int> requested = new List<int>();

        public Chat()
        {
            InitializeComponent();

            var messageThread = new Thread(new ThreadStart(MessageHandler));
            var requestThread = new Thread(new ThreadStart(RequestHandler));
            var statusThread = new Thread(new ThreadStart(StatusHandler));

            messageThread.Start();
            requestThread.Start();
            statusThread.Start();
        }

        private void MessageHandler()
        {
            while (!done)
            {
                if (client != null)
                {
                    var message = client.Receive(ref recieveEndPoint);
                    message = HammDecode(message);
                    var type = (char)message[0];

                    if (type == 'D')
                    {
                        var dataMessage = new DataMessage(message);
                        display.TryAdd(new DisplayKey(dataMessage.timeStamp, dataMessage.index, dataMessage.id), dataMessage);
                        recieved.TryAdd(dataMessage.index, dataMessage);
                        recievedCount = recieved.Count;
                        if (dataMessage.index > highestRecieved)
                        {
                            highestRecieved = dataMessage.index;
                        }
                    }
                    else if (type == 'S')
                    {
                        var statusMessage = new StatusMessage(message);
                        sentByOtherCount = statusMessage.sentCount;
                        recievedByOtherCount = statusMessage.recievedCount;
                    }
                    else if (type == 'R')
                    {
                        var requestMessage = new RequestMessage(message);
                        requested = requestMessage.requested;
                    }

                    UpdateDisplay();
                    stillRecieving = true;
                }
            }
        }

        private void RequestHandler()
        {
            while (!done)
            {
                Thread.Sleep(10);
                if (client != null)
                {
                    if (recievedCount < sentByOtherCount || highestRecieved > recievedCount)
                    {
                        var message = CreateRequestMessage();
                        message = HammEncode(message);
                        client.Send(message, message.Length, endPoint);
                    }

                    if (recievedByOtherCount < sentCount)
                    {
                        foreach (var index in requested)
                        {
                            var success = toSend.TryGetValue(index, out byte[] message);
                            if (success)
                            {
                                client.Send(message, message.Length, endPoint);
                            }
                        }
                    }
                }
            }
        }

        private void StatusHandler()
        {
            while (!done)
            {
                Thread.Sleep(10);
                if (client != null && sentCount != recievedByOtherCount || sentByOtherCount != recievedCount || stillRecieving)
                {
                    var message = CreateStatusMessage();
                    message = HammEncode(message);
                    client.Send(message, message.Length, endPoint);
                    stillRecieving = false;
                }
            }
        }

        private void UpdateDisplay()
        {
            var keys = display.Keys.ToList();
            keys.Sort((a, b) => a.timeStamp - b.timeStamp == 0 ? a.index - b.index : (int)(a.timeStamp - b.timeStamp));

            var text = new StringBuilder();
            var myBlock = new StringBuilder();
            var otherBlock = new StringBuilder();

            var myBlocks = new Dictionary<long, string>();
            var otherBlocks = new Dictionary<long, string>();

            bool setMyBlockStart = true;
            bool setOtherBlockStart = true;

            long myBlockStart = 0;
            long otherBlockStart = 0;

            foreach (var key in keys)
            {
                var data = display.TryGetValue(key, out var dataMessage);
                if (data)
                {
                    if (dataMessage.id == port)
                    {
                        if (setMyBlockStart)
                        {
                            setMyBlockStart = false;
                            myBlock = new StringBuilder();
                            myBlockStart = dataMessage.timeStamp;
                            if (checkBox1.Checked)
                            {
                                myBlock.Append($"\r\nClient { dataMessage.id - 1981 }:\r\n");
                            }
                        }

                        myBlock.Append(dataMessage.message);

                        if (dataMessage.message == "\n")
                        {
                            setMyBlockStart = true;
                            myBlocks[myBlockStart] = myBlock.ToString();
                        }
                    }
                    else
                    {
                        if (setOtherBlockStart)
                        {
                            setOtherBlockStart = false;
                            otherBlock = new StringBuilder();
                            otherBlockStart = dataMessage.timeStamp;
                            if (checkBox1.Checked)
                            {
                                otherBlock.Append($"\r\nClient { dataMessage.id - 1981 }:\r\n");
                            }
                        }

                        otherBlock.Append(dataMessage.message);

                        if (dataMessage.message == "\n")
                        {
                            setOtherBlockStart = true;
                            otherBlocks[otherBlockStart] = otherBlock.ToString();
                        }
                    }
                }
            }

            myBlocks[myBlockStart] = myBlock.ToString();
            otherBlocks[otherBlockStart] = otherBlock.ToString();

            var myKeys = myBlocks.Keys.ToList();
            var otherKeys = otherBlocks.Keys.ToList();

            myKeys.Sort();
            otherKeys.Sort();

            var myIndex = 0;
            var otherIndex = 0;

            while (myIndex < myKeys.Count && otherIndex < otherKeys.Count)
            {
                long min = Math.Min(myKeys[myIndex], otherKeys[otherIndex]);
                if (min == myKeys[myIndex])
                {
                    text.Append(myBlocks[myKeys[myIndex++]]);
                }
                else
                {
                    text.Append(otherBlocks[otherKeys[otherIndex++]]);
                }
            }

            while (myIndex < myKeys.Count)
            {
                text.Append(myBlocks[myKeys[myIndex++]]);
            }

            while (otherIndex < otherKeys.Count)
            {
                text.Append(otherBlocks[otherKeys[otherIndex++]]);
            }

            displayBox.Invoke((MethodInvoker)delegate
            {
                displayBox.Text = text.ToString();

                if (textLength != displayBox.Text.Length)
                {
                    textLength = displayBox.Text.Length;
                    displayBox.SelectionStart = displayBox.Text.Length;
                    displayBox.ScrollToCaret();
                    displayBox.Refresh();
                }
            });
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            Connect();
            Send();
        }

        private void Connect()
        {
            var currentPort = (int)sendPortBox.Value;

            if (currentPort != port)
            {
                port = currentPort;

                client?.Close();
                endPoint = new IPEndPoint(IPAddress.Loopback, port);
                recieveEndPoint = new IPEndPoint(IPAddress.Loopback, port + 2);
                client = new UdpClient(recieveEndPoint);
            }
        }

        private void Send()
        {
            Connect();
            if (client != null)
            {
                var text = chatbox.Text;



                foreach (var c in text)
                {
                    Thread.Sleep(1);
                    sentCount++;

                    if (c == '\n') currentBlock++;

                    var time = DateTimeOffset.Now.ToUnixTimeMilliseconds();
                    var message = CreateDataMessage($"{c}", time);
                    display.TryAdd(new DisplayKey(time, port, sentCount), new DataMessage(message));

                    message = HammEncode(message);

                    toSend.TryAdd(sentCount, message);
                    client.Send(message, message.Length, endPoint);
                }

                chatbox.Text = "";
                UpdateDisplay();
            }
        }

        private byte[] HammEncode(byte[] message)
        {
            var toRet = new byte[message.Length * 2];

            int i = 0;
            foreach (byte b in message)
            {
                var first = (b << 4) >> 4;
                var last = (b >> 4);

                toRet[i] = EncodeHalfByte(first);
                toRet[i + 1] = EncodeHalfByte(last);

                i += 2;
            }

            return toRet;
        }

        private byte EncodeHalfByte(int val)
        {
            byte b = 0;
            var index = 0;
            for (int i = 0; i < 8; i++)
            {
                if (!IsPower(i + 1))
                {
                    b |= (byte)(((val >> index++) & 1) << i);
                }
            }

            for (int i = 0; i < 8; i++)
            {
                if (IsPower(i + 1))
                {
                    var parity = 0;
                    var count = 0;

                    for (int j = i; j < 8; j++)
                    {
                        if (count > i)
                        {
                            count = 0;
                            j += i;
                        }
                        else
                        {
                            parity ^= (b >> j) & 1;
                            count++;
                        }

                    }
                    b |= (byte)(parity << i);
                }
            }

            return b;
        }

        private byte[] HammDecode(byte[] message)
        {
            var toRet = new byte[message.Length / 2];

            for (int i = 0; i < message.Length; i += 2)
            {
                toRet[i / 2] = DecodeHalfBytes(message[i], message[i + 1]);
            }

            return toRet;
        }

        private byte DecodeHalfBytes(byte first, byte second)
        {
            first = CorrectError(first);
            second = CorrectError(second);

            byte firstHalf = ExtractHalfByte(first);
            byte secondHalf = ExtractHalfByte(second);

            return (byte)(firstHalf | (secondHalf << 4));
        }

        private byte CorrectError(byte b)
        {
            var error = 0;

            for (int i = 0; i < 8; i++)
            {
                if (IsPower(i + 1))
                {
                    var parity = 0;
                    var count = 1;

                    for (int j = i + 1; j < 8; j++)
                    {
                        if (count > i)
                        {
                            count = 0;
                            j += i;
                        }
                        else
                        {
                            parity ^= (b >> j) & 1;
                            count++;
                        }

                    }

                    error |= (parity ^ ((b >> i) & 1)) << i;
                }
            }
            if (error != 0)
            {
                var location = 0;
                for (int i = 0; i < 8; i++)
                {
                    if (((error >> i) & 1) == 1)
                    {
                        location += (i + 1);
                    }
                }

                b ^= (byte)(1 << (location - 1));
            }

            return b;
        }

        private byte ExtractHalfByte(byte b)
        {
            byte toRet = 0;
            var index = 0;
            for (int i = 0; i < 8; i++)
            {
                if (!IsPower(i + 1))
                {
                    toRet |= (byte)(((b >> i) & 1) << index++);
                }
            }
            return toRet;
        }

        private bool IsPower(int val)
        {
            var count = 0;
            for (int i = 0; i < 32; i++)
            {
                count += ((val >> i) & 1) == 0 ? 0 : 1;
            }
            return count == 1;
        }

        private void Chat_FormClosing(object sender, FormClosingEventArgs e)
        {
            done = true;
            if (client != null)
            {
                client.Send(new byte[] { 1 }, 1, recieveEndPoint);
                client.Receive(ref recieveEndPoint);
                client.Close();
                client.Dispose();
            }
        }

        private byte[] CreateDataMessage(string message, long time)
        {
            byte[] bytes = new byte[21 + message.Length];
            bytes[0] = (byte)'D';
            Buffer.BlockCopy(BitConverter.GetBytes(sentCount), 0, bytes, 1, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(currentBlock), 0, bytes, 5, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(port), 0, bytes, 9, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(time), 0, bytes, 13, 8);
            Buffer.BlockCopy(Encoding.ASCII.GetBytes(message), 0, bytes, 21, message.Length);
            return bytes;
        }

        private byte[] CreateStatusMessage()
        {
            byte[] bytes = new byte[9];
            bytes[0] = (byte)'S';
            Buffer.BlockCopy(BitConverter.GetBytes(sentCount), 0, bytes, 1, 4);
            Buffer.BlockCopy(BitConverter.GetBytes(recievedCount), 0, bytes, 5, 4);
            return bytes;
        }

        private byte[] CreateRequestMessage()
        {
            var toRequest = new List<int>(Math.Abs(sentByOtherCount - recievedCount));
            for (int i = 0; i < sentByOtherCount; i++)
            {
                if (!recieved.Keys.Contains(i + 1))
                {
                    toRequest.Add(i + 1);
                }
            }
            byte[] bytes = new byte[1 + (toRequest.Count * 4)];
            bytes[0] = (byte)'R';

            for (int i = 0; i < toRequest.Count; i++)
            {
                Buffer.BlockCopy(BitConverter.GetBytes(toRequest[i]), 0, bytes, 1 + (i * 4), 4);
            }

            return bytes;
        }

        struct DataMessage
        {
            public char type;
            public int index;
            public int block;
            public int id;
            public long timeStamp;
            public string message;

            public DataMessage(byte[] bytes)
            {
                type = (char)bytes[0];
                index = BitConverter.ToInt32(bytes, 1);
                block = BitConverter.ToInt32(bytes, 5);
                id = BitConverter.ToInt32(bytes, 9);
                timeStamp = BitConverter.ToInt64(bytes, 13);
                message = Encoding.ASCII.GetString(bytes.Skip(21).ToArray());
            }
        }

        struct StatusMessage
        {
            public char type;
            public int sentCount;
            public int recievedCount;

            public StatusMessage(byte[] bytes)
            {
                type = (char)bytes[0];
                sentCount = BitConverter.ToInt32(bytes, 1);
                recievedCount = BitConverter.ToInt32(bytes, 5);
            }
        }

        struct RequestMessage
        {
            public char type;
            public List<int> requested;

            public RequestMessage(byte[] bytes)
            {
                type = (char)bytes[0];
                requested = new List<int>(bytes.Length / 4);
                for (int i = 1; i < bytes.Length; i += 4)
                {
                    requested.Add(BitConverter.ToInt32(bytes, i));
                }
            }
        }

        struct DisplayKey
        {
            public long timeStamp;
            public int id;
            public int index;

            public DisplayKey(long timeStamp, int id, int index)
            {
                this.timeStamp = timeStamp;
                this.id = id;
                this.index = index;
            }
        }

        private void chatbox_TextChanged(object sender, EventArgs e)
        {
            Send();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDisplay();
        }

        private void sendButton_Click_1(object sender, EventArgs e)
        {
            chatbox.Text += "\r\n";
        }
    }
}
